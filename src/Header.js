import React ,{Component} from 'react'
import './Header.css'

// export default function Header(props) {
//     return (
//         <div className="Header">
//             {props.heading}
//         </div>
//     )
// }



export default class Header extends Component {
    render() {
        return (
            <div className="Header">
               {this.props.heading}
            </div>
        )
    }
}
